global Bordes_asm

white: times 16 db 0xFF

Bordes_asm:
	; rdi=uint8_t *src, rsi=uint8_t *dst, rdx=int width, rcx=int height, r8=int src_row_size, r9=int dst_row_size

	; Según el enunciado, el framework conviete la imágen pasada a escala de grises.
	; Eso significa que hay solo una componente por pixel, luego, cada pixel mide 1 byte:
	; rdx=r8=r9
	push rbp
	mov rbp, rsp

    ; Por las dudas de que haya basura en la parte alta, limpio los parámetros de filas y columnas.
    mov edx, edx
    mov ecx, ecx

    ; Calculo el final de la matriz
    mov rax, rcx ; rax = height
    mov r9, rdx ; salvo rdx
    mul rdx ; rax = filas*cols
    mov rdx, r9 ; restauro rdx despues del mul
    sub rax, rdx; rax = filas*cols - filas
    lea r9, [rdi + rax - 16] ; r9 = apunta a la posición [N-2, M-2] de matriz

	; Me guardo RSI
	mov rbx, rsi

	; Me muevo a 1_1: debo dejar bordes de 1 pixel, luego el primer pixel a procesar es el 1_1.
	lea rdi, [rdi + rdx + 1]
	lea rsi, [rsi + rdx + 1]
	; rdi -> puntero al pixel actual
	; rsi -> puntero al pixel actual (destino)

	; Calculo punteros de filas para usarlos luego.
	mov r10, rdi
	mov r11, rdi
	sub r10, rdx
	add r11, rdx
	; r10 -> puntero al pixel de arriba
	; r11 -> puntero al pixel de abajo

	._loop:
		call Gx
		movdqu xmm5, xmm0
		call Gy
		paddusb xmm0, xmm5

		; Guardo resultado
		movdqu [rsi], xmm0

		add r10, 16
		add rdi, 16
		add r11, 16
		add rsi, 16

		cmp rdi, r9
		jle ._loop

		; Proceso última iteración
		sub r10, 2
		sub rdi, 2
		sub r11, 2
		sub rsi, 2

		call Gx
		movdqu xmm5, xmm0
		call Gy
		paddusb xmm0, xmm5

		; Guardo resultado
		movdqu [rsi], xmm0

	; Recupero RSI
	mov rsi, rbx
	call Marcos

	pop rbp
ret;

Gx:
	push rbp
	mov rbp, rsp

	; Agarro los pixeles de arriba (de a 16)
	movdqu xmm1, [r10 - 1] 
	movdqu xmm2, [r10] 
	movdqu xmm3, [r10 + 1]

	; Agarro los pixeles del medio (de a 16)
	movdqu xmm4, [rdi - 1] 
	; movdqu xmm5, [rdi] 
	movdqu xmm6, [rdi + 1]

	; Agarro los pixeles de abajo (de a 16)
	movdqu xmm7, [r11 - 1] 
	movdqu xmm8, [r11] 
	movdqu xmm9, [r11 + 1]

	; Extiendo la precisión de todos
	movdqu xmm10, xmm1
	movdqu xmm11, xmm3
	movdqu xmm12, xmm4
	movdqu xmm13, xmm6
	movdqu xmm14, xmm7
	movdqu xmm15, xmm9
	
	pxor xmm0, xmm0

	punpcklbw xmm1, xmm0
	punpcklbw xmm3, xmm0
	punpcklbw xmm4, xmm0
	punpcklbw xmm6, xmm0
	punpcklbw xmm7, xmm0
	punpcklbw xmm9, xmm0

	punpckhbw xmm10, xmm0
	punpckhbw xmm11, xmm0
	punpckhbw xmm12, xmm0
	punpckhbw xmm13, xmm0
	punpckhbw xmm14, xmm0
	punpckhbw xmm15, xmm0

	; Multiplico 4 (por 2)
	psllw xmm4, 1
	psllw xmm12, 1

	; Multiplico 6 (por 2)
	psllw xmm6, 1
	psllw xmm13, 1

	; Sumo 3, 6 y 9
	paddw xmm3, xmm6
	paddw xmm3, xmm9
	paddw xmm11, xmm13
	paddw xmm11, xmm15

	; Resto 1, 4 y 7
	psubw xmm3, xmm1
	psubw xmm3, xmm4
	psubw xmm3, xmm7
	psubw xmm11, xmm10
	psubw xmm11, xmm12
	psubw xmm11, xmm14
	
	; Invierto los negativos
	pxor xmm0, xmm0
	pcmpgtw xmm0, xmm3
	pand xmm0, xmm3
	psubw xmm3, xmm0
	psubw xmm3, xmm0
	
	pxor xmm0, xmm0
	pcmpgtw xmm0, xmm11
	pand xmm0, xmm11
	psubw xmm11, xmm0
	psubw xmm11, xmm0

	; Empaqueto saturando
	packuswb xmm3, xmm11

	movdqu xmm0, xmm3

	pop rbp
ret

Gy:
	push rbp
	mov rbp, rsp

	; Agarro los pixeles de arriba (de a 16)
	movdqu xmm1, [r10 - 1] 
	movdqu xmm2, [r10] 
	movdqu xmm3, [r10 + 1]

	; Agarro los pixeles del medio (de a 16)
	movdqu xmm4, [rdi - 1] 
	; movdqu xmm5, [rdi] 
	movdqu xmm6, [rdi + 1]

	; Agarro los pixeles de abajo (de a 16)
	movdqu xmm7, [r11 - 1] 
	movdqu xmm8, [r11] 
	movdqu xmm9, [r11 + 1]

	; Extiendo la precisión de todos
	movdqu xmm10, xmm1
	movdqu xmm11, xmm2
	movdqu xmm12, xmm3
	movdqu xmm13, xmm7
	movdqu xmm14, xmm8
	movdqu xmm15, xmm9
	
	pxor xmm0, xmm0

	punpcklbw xmm1, xmm0
	punpcklbw xmm2, xmm0
	punpcklbw xmm3, xmm0
	punpcklbw xmm7, xmm0
	punpcklbw xmm8, xmm0
	punpcklbw xmm9, xmm0

	punpckhbw xmm10, xmm0
	punpckhbw xmm11, xmm0
	punpckhbw xmm12, xmm0
	punpckhbw xmm13, xmm0
	punpckhbw xmm14, xmm0
	punpckhbw xmm15, xmm0

	; Multiplico 4 (por 2)
	psllw xmm2, 1
	psllw xmm11, 1

	; Multiplico 6 (por 2)
	psllw xmm8, 1
	psllw xmm14, 1

	; Sumo 7, 8 y 9
	paddw xmm7, xmm8
	paddw xmm7, xmm9
	paddw xmm13, xmm14
	paddw xmm13, xmm15

	; Resto 1, 2 y 3
	psubw xmm7, xmm1
	psubw xmm7, xmm2
	psubw xmm7, xmm3
	psubw xmm13, xmm10
	psubw xmm13, xmm11
	psubw xmm13, xmm12
	
	; Invierto los negativos
	pxor xmm0, xmm0
	pcmpgtw xmm0, xmm7
	pand xmm0, xmm7
	psubw xmm7, xmm0
	psubw xmm7, xmm0
	
	pxor xmm0, xmm0
	pcmpgtw xmm0, xmm13
	pand xmm0, xmm13
	psubw xmm13, xmm0
	psubw xmm13, xmm0

	; Empaqueto saturando
	packuswb xmm7, xmm13

	movdqu xmm0, xmm7

	pop rbp
ret

Marcos:
	push rbp
	mov rbp, rsp

	; Calculo última fila
    mov rax, rcx ; rax = height
    mov r9, rdx ; salvo rdx
    mul rdx ; rax = filas*cols
    mov rdx, r9 ; restauro rdx despues del mul
    sub rax, rdx; rax = filas*cols - columnas

	movdqu xmm0, [white]

	; Guardo rcx
	mov r9, rcx
	; Guardo rsi
	mov rbx, rsi

	mov rcx, rdx
	shr rcx, 4
	
    ._marcoSuperiorInferior:
		movdqu [rsi], xmm0
		movdqu [rsi + rax], xmm0
		add rsi, 16
		loop ._marcoSuperiorInferior

	; Recupero rcx
	mov rcx, r9
	; Recupero rsi
	mov rsi, rbx

	._marcosLaterales:
		mov byte [rsi], 255
		mov byte [rsi + rdx - 1], 255
		add rsi, rdx
		loop ._marcosLaterales

	pop rbp
ret