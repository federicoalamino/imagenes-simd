global Nivel_exp

mask_0: times 32 db 1
ones: times 32 db 255

Nivel_exp:
	; rdi=uint8_t *src, rsi=uint8_t *dst, rdx=int width, rcx=int height, r8=int src_row_size, r9=int dst_row_size, PILA=int n
	push rbp
	mov rbp, rsp

	; Cargo la máscara con 1s y shifteo tantas veces como indique el parámetro n
	._mask_level:
		mov al, [rbp + 16]
		vmovdqu ymm3, [mask_0]
		._shift:
			cmp al, 0
			je ._rows
			vpsllw ymm3, 1
			dec al
			jmp ._shift

	._rows:
		mov r12, 0
		._columns:
			vmovdqu ymm0, [rsi + r12]	; Traigo 4 pixeles.
			vmovdqu ymm1, ymm3			; Cargo la máscara.
			vpand ymm1, ymm0 			; Dejo en 1 solo los bits de cada componente que ya estaban en 1 y coinciden con los de la máscara.
			vpxor ymm2, ymm2
			vpcmpeqb ymm1, ymm2 		; Por cada byte (canal) chequeo si es igual a 0 (osea si no tiene ningún bit en 1), en ese caso pongo todos 1s.
			vmovdqu ymm2, [ones]		; Cargo unos.
			vpxor ymm1, ymm2			; Invierto xmm1 (dejo las componentes correctas saturadas, el resto en 0s).
			vmovdqu [rsi + r12], ymm1

			add r12, 32
			cmp r12, r9
			jne ._columns

		lea rsi, [rsi + r9]
		loop ._rows

	pop rbp
ret
