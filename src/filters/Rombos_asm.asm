global Rombos_asm

data:
mask_size  : dw  0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040
mask_size_sub : dw  0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040, 0x0040
mask_size2 : dw  0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020, 0x0020
mask_size16 : dw  0x0004, 0x0004, 0x0004, 0x0004, 0x0004, 0x0004, 0x0004, 0x0004
mask_size16_sub: dw 0x0005, 0x0005, 0x0005, 0x0005, 0x0005, 0x0005, 0x0005, 0x0005
init_mask: dw  0x0000, 0x0000, 0x0000, 0x0001, 0x0000, 0x0000, 0x0000, 0x0001
incremento_columna: dw  0x0000, 0x0002, 0x0000, 0x0002, 0x0000, 0x0002, 0x0000, 0x0002 ;incremento de a 2 columnas.
incremento_fila: dw  0x0001, 0x0000, 0x0001, 0x0000, 0x0001, 0x0000, 0x0001, 0x0000
incremento_fila_0: dw  0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000, 0xFFFF, 0x0000 ;lo uso para poner en 0 las columnas, cuando cambio de fila.
mask_1 : dw 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001, 0x0001
alpha_mask:db 0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0xFF

text:
Rombos_asm:
push rbp
mov rbp, rsp
;rdi = src
;rsi = dst
;rdx = width = ancho en pix
;rcx = height = alto en pix
movdqu xmm0, [init_mask]
movdqu xmm1, [incremento_fila]
movdqu xmm3, [incremento_fila_0]
movdqu xmm4, [incremento_columna]
movdqu xmm5, [mask_size2]
movdqu xmm6, [mask_size16]
movdqu xmm7, [mask_size]
pxor xmm2, xmm2

.cambioFila:
mov r8, rdx
movdqu xmm0, [init_mask]
paddw xmm0, xmm2			 ;xmm0 va a tener seteado los valores partiendo de la columna 1 con su respectiva fila.

.cicloPrincipal:
;CALCULO II, JJ
movdqu xmm8, xmm0 			;muevo las coordenadas del pixel actual y de los 3 que siguen a xmm8.

.modulo:
movdqu xmm9, xmm8
pcmpgtw xmm9, xmm7                      ;veo que word de xmm8 esta en modulo. Para las que estan en "modulo" seteo 0, las que son mayor seteo 0xFFF.
pand xmm9, xmm7                         ;Seteo 0x20 en los lugares que debo restar.
psubw xmm8, xmm9                        ;resto.
ptest xmm9, xmm9                        ;veo si estan todos en modulo o no
jz .sigo
jmp .modulo


.sigo:
pxor xmm10, xmm10
pxor xmm11, xmm11
movdqu xmm9, xmm5
psubw xmm9, xmm8 	;xmm9 = (size/2) - (ii/jj % size)
pcmpgtw xmm10, xmm9	;xmm10 = [(0 > ((size/2) - (ii/jj % size)) ? ff :00]
pand xmm10, xmm9 	;xmm10 = quedan valores de xmm9 <0, los >0 quedan en 0
psubw xmm11, xmm10 	;xmm11 = valores de xmm9 <0 ahora quedan positivos
paddw xmm9, xmm11
paddw xmm9, xmm11 	;Le sumo dos veces el valor asì me queda en positivo


;en xmm9 tengo el ii y jj para los 4 pixeles. Registros reutilizablescon basura actualmente xmm10, xmm11, xmm8
;CALCULO X

phaddsw xmm9, xmm9 ;me sirven los 64 bits principales.

psubw xmm9, xmm5 ;xmm9 =  [ii+jj - (size/2)]
psrldq xmm9, 8
pslldq xmm9, 8 ;la parte alta de xmm9 tiene lo que necesito, los 64 bitsrestantes son 0

;movdqu xmm8, xmm6 ;xmm8 = size/16
movdqu xmm8, [mask_size16_sub]  
pcmpgtw xmm8, xmm9 ; xmm8 = [(size/16 > [ii+jj - (size/2)]) ? ff : 00]
pand xmm8, xmm9
movdqu xmm9, xmm8
paddw xmm9, xmm9


;en xmm9 tengo [x1,x2,x3,x4, 0, 0, 0, 0]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Operaciones principales;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;xmm12, xmm13, xmm14, xmm15
pxor xmm14, xmm14
pxor xmm12, xmm12
movdqu xmm12, xmm9
movdqu xmm13, xmm9
psrldq xmm13, 14        ;xmm13 = [0, 0, 0, 0, 0, 0, 0, x1]
pslldq xmm13, 14        ;xmm13 = [x1, 0, 0, 0, 0, 0, 0, 0]
movdqu xmm12, xmm13     ;xmm12 = [x1, 0, 0, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, x1, 0, 0, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, 0, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, 0, x1, 0, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, 0, 0, x1, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, x1, 0, 0, 0, 0]

movdqu xmm12, xmm9      ;xmm12 = [x1,x2,x3,x4, 0, 0, 0, 0]
pslldq xmm12, 2         ;xmm12 = [x2,x3,x4, 0, 0, 0, 0, 0]
psrldq xmm12, 14        ;xmm12 = [0, 0, 0, 0, 0, 0, 0, x2]
pslldq xmm12, 6         ;xmm12 = [0, 0, 0, 0, x2, 0, 0, 0]
pxor xmm14, xmm14       ;xmm14 <- ceros
movdqu xmm14, xmm12     ;xmm14 <- xmm12
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, x2, 0, 0]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, 0, 0]
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, 0, x2, 0]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, x2, 0]
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, 0, 0, x2]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, x2, x2]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, x1, x2, x2, x2, x2]
pxor xmm14, xmm14       ;xmm14 <- ceros

movdqu xmm15, [rdi]
punpcklbw xmm15, xmm14 ; XMM15 =  [0B/0G/0R/0A 0B/0G/0R/0A]
paddw xmm15, xmm13
packuswb xmm15, xmm15
movdqu xmm14, [alpha_mask]
por xmm15, xmm14
movdqu [rsi], xmm15

;iteraciòn
sub r8, 2
paddw xmm0, xmm4
cmp rcx, 1
je .verif
.sigoFij:
add rdi, 8
add rsi, 8
cmp r8, 0
je .cambioF
jmp .cicloPrincipal

.verif:
cmp r8, 2
je .patch
jmp .sigoFij

.cambioF:
paddw xmm2, xmm1
dec rcx
cmp rcx, 0
je .fin
jmp .cambioFila

.patch: 
movdqu xmm8, xmm0 			;muevo las coordenadas del pixel actual y de los 3 que siguen a xmm8.

.modulo1:
movdqu xmm9, xmm8
pcmpgtw xmm9, xmm7                      ;veo que word de xmm8 esta en modulo. Para las que estan en "modulo" seteo 0, las que son mayor seteo 0xFFF.
pand xmm9, xmm7                         ;Seteo 0x20 en los lugares que debo restar.
psubw xmm8, xmm9                        ;resto.
ptest xmm9, xmm9                        ;veo si estan todos en modulo o no
jz .sigo1
jmp .modulo1

.sigo1:
pxor xmm10, xmm10
pxor xmm11, xmm11
movdqu xmm9, xmm5
psubw xmm9, xmm8 	;xmm9 = (size/2) - (ii/jj % size)
pcmpgtw xmm10, xmm9	;xmm10 = [(0 > ((size/2) - (ii/jj % size)) ? ff :00]
pand xmm10, xmm9 	;xmm10 = quedan valores de xmm9 <0, los >0 quedan en 0
psubw xmm11, xmm10 	;xmm11 = valores de xmm9 <0 ahora quedan positivos
paddw xmm9, xmm11
paddw xmm9, xmm11 	;Le sumo dos veces el valor asì me queda en positivo
 
phaddsw xmm9, xmm9 ;me sirven los 64 bits principales.

psubw xmm9, xmm5 ;xmm9 =  [ii+jj - (size/2)]
psrldq xmm9, 8
pslldq xmm9, 8 ;la parte alta de xmm9 tiene lo que necesito, los 64 bitsrestantes son 0

movdqu xmm8, [mask_size16_sub]  
pcmpgtw xmm8, xmm9 ; xmm8 = [(size/16 > [ii+jj - (size/2)]) ? ff : 00]
pand xmm8, xmm9
movdqu xmm9, xmm8
paddw xmm9, xmm9

pxor xmm14, xmm14
pxor xmm12, xmm12
movdqu xmm12, xmm9
movdqu xmm13, xmm9
psrldq xmm13, 14        ;xmm13 = [0, 0, 0, 0, 0, 0, 0, x1]
pslldq xmm13, 14        ;xmm13 = [x1, 0, 0, 0, 0, 0, 0, 0]
movdqu xmm12, xmm13     ;xmm12 = [x1, 0, 0, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, x1, 0, 0, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, 0, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, 0, x1, 0, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, 0, 0, 0, 0, 0]
psrldq xmm12, 2         ;xmm12 = [0, 0, 0, x1, 0, 0, 0, 0]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, x1, 0, 0, 0, 0]

movdqu xmm12, xmm9      ;xmm12 = [x1,x2,x3,x4, 0, 0, 0, 0]
pslldq xmm12, 2         ;xmm12 = [x2,x3,x4, 0, 0, 0, 0, 0]
psrldq xmm12, 14        ;xmm12 = [0, 0, 0, 0, 0, 0, 0, x2]
pslldq xmm12, 6         ;xmm12 = [0, 0, 0, 0, x2, 0, 0, 0]
pxor xmm14, xmm14       ;xmm14 <- ceros
movdqu xmm14, xmm12     ;xmm14 <- xmm12
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, x2, 0, 0]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, 0, 0]
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, 0, x2, 0]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, x2, 0]
psrldq xmm14, 2         ;xmm14 = [0, 0, 0, 0, 0, 0, 0, x2]
paddw xmm12, xmm14      ;xmm12 = [0, 0, 0, 0, x2, x2, x2, x2]
paddw xmm13, xmm12      ;xmm13 = [x1, x1, x1, x1, x2, x2, x2, x2]
pxor xmm14, xmm14       ;xmm14 <- ceros


movdqu xmm15, [rdi]
punpckhbw xmm15, xmm14 ; XMM15 =  [0B/0G/0R/0A 0B/0G/0R/0A]
paddw xmm15, xmm13
packuswb xmm15, xmm15
movdqu xmm14, [alpha_mask]
por xmm15, xmm14
movdqu xmm14, [rsi]
psrldq xmm14, 8
pslldq xmm15, 8
paddw xmm15, xmm14
movdqu [rsi], xmm15

.fin:
pop rbp
ret
