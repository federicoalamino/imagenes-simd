global Nivel_asm

mask_0: times 16 db 1
ones: times 16 db 255

Nivel_asm:
	; rdi=uint8_t *src, rsi=uint8_t *dst, rdx=int width, rcx=int height, r8=int src_row_size, r9=int dst_row_size, PILA=int n
	push rbp
	mov rbp, rsp

	; Cargo la máscara con 1s y shifteo tantas veces como indique el parámetro n
	._mask_level:
		mov al, [rbp + 16]
		movdqu xmm3, [mask_0]
		._shift:
			cmp al, 0
			je ._rows
			psllw xmm3, 1
			dec al
			jmp ._shift

	._rows:
		mov r12, 0
		._columns:
			movdqu xmm0, [rsi + r12]; Traigo 4 pixeles.
			movdqu xmm1, xmm3		; Cargo la máscara.
			pand xmm1, xmm0 		; Dejo en 1 solo los bits de cada componente que ya estaban en 1 y coinciden con los de la máscara.
			pxor xmm2, xmm2 		; Cargo ceros.
			pcmpeqb xmm1, xmm2 		; Por cada byte (canal) chequeo si es igual a 0 (osea si no tiene ningún bit en 1), en ese caso pongo todos 1s.
			movdqu xmm2, [ones]		; Cargo unos.
			pxor xmm1, xmm2			; Invierto xmm1 (dejo las componentes correctas saturadas, el resto en 0s).
			movdqu [rsi + r12], xmm1

			add r12, 16
			cmp r12, r9
			jne ._columns

		lea rsi, [rsi + r9]
		loop ._rows

	pop rbp
ret
