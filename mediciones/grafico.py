#!/usr/bin/env python3
import matplotlib.pyplot as plt
import sys

if len(sys.argv) != 3:
    print('Uso: %s filtro salida' % (sys.argv[0],))
    exit()

lines = [line.split() for line in sys.stdin.readlines()]

etiquetas = [line[0] for line in lines]
medias_asm = [float(line[1]) for line in lines]
medias_asm_exp = [float(line[3]) for line in lines]
medias_O0 = [float(line[5]) for line in lines]
medias_O3 = [float(line[7]) for line in lines]
errores_asm = [float(line[2]) for line in lines]
errores_asm_exp = [float(line[4]) for line in lines]
errores_O0 = [float(line[6]) for line in lines]
errores_O3 = [float(line[8]) for line in lines]

filtro = sys.argv[1]
salida = sys.argv[2]

plt.xticks(range(0, len(etiquetas), 1), rotation=30, fontsize=8)
plt.yscale('log')
plt.errorbar(etiquetas, medias_O0, yerr=errores_O0, label='C (O0)')
plt.errorbar(etiquetas, medias_O3, yerr=errores_O3, label='C (O3)')
plt.errorbar(etiquetas, medias_asm, yerr=errores_asm, label='ASM')
plt.errorbar(etiquetas, medias_asm_exp, yerr=errores_asm_exp, label='ASM(experimentacion)')
plt.xlabel('Tamaños')
plt.ylabel('Ciclos del procesador')
plt.title('Filtro ' + filtro)
plt.legend()
plt.savefig(fname=salida, format='pdf')
