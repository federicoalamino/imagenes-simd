# Los nombres de las fotos de entrada (sin directorio ni extensión)
# entradas2=$(shell basename -s .bmp $(wildcard entradas/*.bmp))

# resoluciones="echo {64..768..32}x{64..768..32}" (no se cómo hacerlo inline)
resoluciones=$(shell cat resoluciones)
res_exp1=32x18 64x37 128x75 256x150 512x300 1024x600 2048x1200

optimizaciones=O0 O3

filtros=c-Rombos c-Nivel c-Bordes
filtros+=asm-Rombos asm-Nivel asm-Bordes
filtros+=exp-Rombos exp-Nivel exp-Bordes
# Filtros que se usan para los gráficos simples (tienen implementación asm y c)
filtros_grafico=Rombos Nivel Bordes

iteraciones?=500
param_nivel?=7
retest=
graficos_exp1= # Se rellenan automáticamente
graficos_exp2= # Se rellenan automáticamente
graficos_exp3= # Se rellenan automáticamente
graficos_exp4= # Se rellenan automáticamente

all: FORCE binarios fotos experimentos graficos

clean:
	rm -f entradas/*.bmp graficos/* salidas/*.bmp
	$(MAKE) -C binarios clean

cleandata:
	rm -f data/*

FORCE:
	mkdir -p entradas
	mkdir -p salidas
	mkdir -p data
	mkdir -p graficos

binarios:
	$(MAKE) -C binarios

fotos: $(foreach resolucion,$(resoluciones), entradas/$(resolucion).bmp)

entradas/%.bmp: img_exp.bmp
	convert -type truecolor -resize $*\! $^ $@

experimentos: $(foreach resolucion,$(resoluciones), \
				$(foreach filtro,$(filtros), \
					$(foreach optimizacion,$(optimizaciones), \
						data/$(resolucion).$(filtro).$(optimizacion).stats)))

# SALIDA res filtro optimizacion
define SALIDA =
data/$(1).$(2).$(3).txt: $(if $(retest),entradas/$(1).bmp binarios/$(3)/tp2)
	binarios/$(3)/tp2 --simple -o salidas -t $(iteraciones) -i $(subst -, ,$(2)) entradas/$(1).bmp $(param_nivel) > $$@
endef

$(foreach resolucion,$(resoluciones), \
  	$(foreach filtro,$(filtros), \
    	$(foreach optimizacion,$(optimizaciones), \
      $(eval $(call SALIDA,$(resolucion),$(filtro),$(optimizacion))))))

data/%.stats: data/%.txt stats.gnuplot
	./stats.gnuplot $^ > $@

graficos/%.pdf: $(foreach res,$(resoluciones), data/$(res).asm-%.O0.stats data/$(res).exp-%.O0.stats \
			 $(foreach opt,$(optimizaciones), \
			   data/$(res).c-%.$(opt).stats))
	for res in $(resoluciones); do \
		echo $$res $$(cat data/$$res.asm-$*.O0.stats) \
		     $$(cat data/$$res.exp-$*.O0.stats) \
		     $$(cat data/$$res.c-$*.O0.stats) \
		     $$(cat data/$$res.c-$*.O3.stats); \
	done | ./grafico.py $* $@;

graficos_exp1+=$(foreach filtro,$(filtros_grafico), \
	         graficos/$(filtro).pdf)

graficos: $(graficos_exp1)

.PRECIOUS: data/%.txt
.PHONY: clean binarios all fotos experimentos graficos