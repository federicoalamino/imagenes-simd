# Trabajo practico realizado en FCEN-UBA (CS. de la Computación)



## Informe
Se encuentra un informe realizado del trabajo en la solapa informes, hecho con LaTeX en la web Overleaf

## Doc
Documentación dada por la facultad. Aquí se puede ver lo que pedia el enunciado y la finalidad del aprendizaje al realizar dicho trabajo practico.

## SRC
Finalmente en la carpeta SRC se encuentra lo que al trabajo respecta. Filtros implementados en ASM, makefile y su comparación con el mismo filtro implementado en C.

## Mediciones
Se agregó una carpeta /mediciones con todo lo necesario para reproducir tanto las mediciones como los gráficos.
Para correr ejecutar: `make` en dicha carpeta.

